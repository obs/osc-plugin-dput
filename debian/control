Source: osc-plugins-dput
Section: devel
Priority: optional
Maintainer: Héctor Orón Martínez <zumbi@debian.org>
Uploaders:
 Andrew Lee (李健秋) <ajqlee@debian.org>,
 Andrej Shadura <andrewsh@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 osc (>= 0.167.1),
 python3,
 python3-debian,
 python3-setuptools
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://gitlab.collabora.com/obs/osc-plugin-dput
Vcs-Browser: https://salsa.debian.org/debian/osc-plugin-dput
Vcs-Git: https://salsa.debian.org/debian/osc-plugin-dput.git

Package: osc-plugin-dput
Architecture: all
Depends:
 osc (>= 0.167.1),
 ${python3:Depends},
 ${misc:Depends}
Provides: osc-plugins-dput
Replaces: osc-plugins-dput
Breaks: osc-plugins-dput (<= 20200202+git31b4d60-1)
Description: dput plugin for OpenSUSE (buildsystem) commander
 This package contains the dput plugin for OpenSUSE (buildsystem) commander.
 .
 The dput plugin is designed to be able to quickly upload a random
 Debian source package without the need of doing a checkout, adding
 the source, removing the old files and committing it again.
 .
 See 'osc help dput' for details.

Package: osc-plugins-dput
Section: oldlibs
Architecture: all
Depends:
 osc-plugin-dput
Description: dput plugin for osc (transitional package)
 The dput plugin is designed to be able to quickly upload a random
 Debian source package without the need of doing a checkout, adding
 the source, removing the old files and committing it again.
 .
 See 'osc help dput' for details.
 .
 This is a transitional package. It can safely be removed.
